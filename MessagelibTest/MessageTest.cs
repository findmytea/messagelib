using Messagelib;
using Xunit;

namespace MessagelibTest
{
    public class MessageTest
    {
        [Fact(DisplayName = "Check that the correct delivery message is returned.")]
        public void DeliverMessage()
        {
            Assert.Equal("Hello, World!", new Message().Deliver());
        }
    }
}
